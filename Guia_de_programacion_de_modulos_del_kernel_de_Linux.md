https://github.com/sergiocollado/LKMPG/blob/master/4.9.11/LKMPG-4.9.11.org

GUÍA DE PROGRAMACIÓN DE MÓDULOS DEL KERNEL DE LINUX

PRESENTACIÓN

Esta guía de programación de módulos del kernel de linux es un libro libre, y se puede reproducir y/o modificar bajo los términos y condiciones de Open Software License, version 3.0.

Este texto se distribuye con el propósito de ser útil, pero sin ninguna garantía, e incluso sin la garantía de mercantibilidad o adecuación para un propósito particular.

El autor anima a la amplia distribución de este texto para uso personal o comercial, siempre que la especificación de copyright se mantenga intacta y se ajuste a lo indicado en la licencia Open Software License. Resumiendo, se puede copiar y distribuir este texto sin coste alguno o por beneficio. No se requieren permisos explícitos del autor para la reproducción de este texto en ningún medio, físico, electrónico o digital.

Los trabajos derivados y traducciones de este texto, se deben estar bajo la licencia Open Software License, y la especificación del copyright original ha de permanecer intacta. Si se añade nuevo contenido al texto, se debe aportar el código fuente para posibles revisiones. Por favor disponga de las revisiones y actualizaciones disponibles directamente al gestor de la documentación, Peter Jay Salzman p@dirac.org. Esto permitirá incluir todas las actualizaciones y proveer de versiones consistentes para la comunidad Linux.

Si se publica o distribuye este libro de manera comercial, donaciones, royalties, y/o copias impresas son gratamente aceptadas por el autor y el proyecto de documentación del Linux, Linux Documentation Project (LDP). Esta contribución muestra su apoyo para el software libre y LDP. Cualquier pregunta o duda, por favor, contacte con la dirección anteriormente indicada.

AUTORIA:

La guía de programación de módulos del kernel de linux, en inglés: “The Linux Kernel Module Programming Guide”, fue originalmente escrita para el kernel 2.2 por Ori Pomerantz. Posteriormente, Ori careció del tiempo para mantener el documento. Después de todo el kernel de Linux, es un blanco móvil. Peter Jay Salzman asumió el mantenimiento y actualización para el kernel 2.4. Con el tiempo Peter tampoco pudo continuar con el desarrollo del kernel 2.6. Así que Michael Burian tomó el relevo y co-mantuvo el documento para el kernel 2.6. Bob Mottram actualizó los ejemplos para el kernel 3.8 y posteriores. Añadió el capítulo sobre Sysfy y actualizó y/o modificó otros capítulos.

NOTAS Y VERSIONES:

El kernel de Linux está en constante desarrollo. Así que siempre ha estado presente la pregunta de si LKMPG debería retirar información anticuada o mantenerla por motivos históricos. Michael Burian, decidió crear una nueva rama del LKMPG para cada nueva versión estable del kernel. Así que LKMPG 4.9.x está destinado al kernel 4.9.x de Linux y LKMPG 2.6.x está dirigida al kernel 2.6 de Linux. No se intenta mantener la información histórica. Cualquier persona interesada en esto debería leer la versión adecuada de LKMPG.

El código fuente y comentarios deberían servir para la mayoría de arquitecturas, pero no se puede afirmar con certeza. Una excepción clara es el capítulo de Interruptores, que solo es válido para la arquitectura x86.

RECONOCIMIENTOS:

Las siguientes personas han contribuido con correcciones o propuestas interesantes: Ignacio Martin, David Porter, Daniele Paolo Scarpazza, Dimo Velev, Francois Audeon, Horst Schirmeier, Bob Mottram and Roman Lakeev.

¿QUÉ ES UN MÓDULO DEL KERNEL?

¿Quiere escribir un módulo para el kernel? Conoce C y ha escrito unos cuantos programas que se ejecutan como procesos, y ahora quiere llegar donde está la acción de verdad … donde un solo puntero mal apuntado puede hacer desaparecer todo su sistema de archivos y un core-dump, significa un reiniciar el sistema (reboot)

Pero ¿qué es exactamente un módulo para el kernel? Los módulos son piezas de código que se pueden cargar y descargar en el kernel, a voluntad. Estos extienden la funcionalidad del kernel sin que sea necesario reiniciar el sistema. Por ejemplo, un módulo típico es el gestor de periféricos (device driver), que permite al kernel acceder al hardware conectado al sistema. Sin módulos todo serían kernels monolíticos y añadiríamos nuevas funcionalidades directamente en la imagen del núcleo. Además de tener kernels que ocuparían mucho, esto tendría la desventaja de hacer que tuviéramos que reconstruir y reiniciar el kernel cada vez que quisiéramos nuevas funcionalidades. 

PAQUETE DE MÓDULOS DEL KERNEL

Las distribuciones de Linux proveen de los siguientes comandos: *modprobe*, *insmod*, y *depmod* dentro de un módulo.

O en Debian:

```bash
sudo apt-get install build-essential kmod
```

O en Parabola.

```bash
sudo pacman -S gcc kmod
```

¿QUÉ MODULOS POSEE MI KERNEL?
	
El comando *lsmod* nos indica que módulos están en nuestro kernel
	
```
sudo lsmod
```
	
Los módulos, se guardan el directorio /proc/modules, así que leyendo ese directorio 
también podemos identificar qué módulos posee nuestro kernel
	
```
sudo cat /proc/modules
```
	
La lista puede ser bastante extensa, así que si se quiere mirar si existe un módulo en concreto, por ejemplo el módulo ‘fat’, se puede buscar mediante el comando:
	
```
sudo lsmod | grep fat
```

¿ES NECESARIO DESCARGAR Y COMPILAR EL KERNEL?
	
Para seguir esta guia no es realmente necesario. Sin embargo, es recomendable que si se reproducen los ejemplos, estos se ejecuten en una máquina virtual o en un entorno de test, para evitar posibles inconvenientes con el sistema.
	
ANTES DE COMENZAR:
	
Antes de comenzar con el código, hay ciertas aclaraciones: cada persona posee un sistema propio, diferente y a su estilo. Hacer que el primer programa "Hola Mundo!" compile, y funcione correctamente, puede complicarse. Por suerte, una vez superada la dificultad inicial, el desarrollo posterior suele ser bastante más asequible.

MODVERSIONING.
	
Un módulo que se compila para un kernel determinado, no se cargará si se intenta integrar en otro kernel, a menos que se especifique CONFIG_MODVERSIONS el el kernel. No se tocará el tema del versionado de módulos, hasta más adelante en este texto. Hasta que se explique el modversioning, los ejemplos de esta guía, puede que no funcionen si se utiliza un kernel con el modversioning activado. De todas formas, la mayoría de distribuciones de linux, vienen con el modversioning activado. En caso de que se tenga problemas al cargar los módulos, debido a errores de versiones, compile el kernel con el modversioning desactivado.

USANDO X

Es altamente recomendable, que escriba, compile y cargue los ejemplos presentados en esta guía. Igualmente recomendable, es que realice esto usando una consola, no usando X.

Los módulos, no pueden imprimir en la pantalla con un printf(), pero sin embargo, sí que pueden guardar información en logs y warnings, que al final, sí que acaba mostrándose, pero únicamente en la consola. Si ejecuta un 'ismod' desde un xterm, la 
los registros (logs) y avisos (warnings) se guardarán, pero únicamente en 'systemd'. No se podrán ver a menos que explícitamente se mire en el 'journalctl'. Para tener acceso inmediato a esta información, se ha de trabajar en la consola.



LICENCIA Y DOCUMENTACIÓN DEL MÓDULO:
	
Honestamente, ¿quíen carga o incluso se interesa sobre módulos propietarios? - Si es su caso, entonces puede que haya visto algo como esto:
	
```bash
# insmod xxxxx.otroWarning: loading xxxxx.ko will taint the kernel: no license
See http://www.tux.org/lkml/#export-tainted for information about tainted modules
Module xxxxx loaded, with warnings
```
	
Se puede usar algunas macros para indicar la licencia de su módulo. Algunos ejemplos son "GPL", "GPL v2", "GPL and additional rights", "Dual BSD/GPL", "Dual MIT/GPL", and "Propietary". Estas macros están definidas en el archivo: linux/module.h
	
Para referenciar que liciencia está usando se usa la macro: MODULE_LICENSE. Esta y algunas otras macros que ayudan a describir el módulo, se ilustran en el siguiente ejemplo:

```C
/*
 *  hello-4.c - Demonstrates module documentation.
 */
#include <linux/module.h>       /* Requerido por todos los módulos*/
#include <linux/kernel.h>       /* Requerido para KERN_INFO */
#include <linux/init.h>         /* Requerido para las MACROS*/

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Bob Mottram");
MODULE_DESCRIPTION("A sample driver");
MODULE_SUPPORTED_DEVICE("testdevice");

static int __init init_hello_4(void)
{
        printk(KERN_INFO "Hola Mundo! 4\n");
        return 0;
}

static void __exit cleanup_hello_4(void)
{
        printk(KERN_INFO "Adiós Mundo! 4\n");
}

module_init(init_hello_4);
module_exit(cleanup_hello_4);
```
		

USANDO X

Es altamente recomendable, que escriba, compile y cargue los ejemplos presentados en esta guía. Igualmente recomendable, es que realice esto usando una consola, no usando X.

Los módulos, no pueden imprimir en la pantalla con un printf(), pero sin embargo, sí que pueden guardar información en logs y warnings, que al final, sí que acaba mostrándose, pero únicamente en la consola. Si ejecuta un 'ismod' desde un xterm, la 
los registros (logs) y avisos (warnings) se guardarán, pero únicamente en 'systemd'. No se podrán ver a menos que explícitamente se mire en el 'journalctl'. Para tener acceso inmediato a esta información, se ha de trabajar en la consola.

CABECERAS (HEADERS)

Antes de comenzar a desarrollar, se han de instalar las cabeceras (headers) en el sistema.

Para Parabola GNU/linux:

```bash
sudo pacman -S linux-libre-headers
```

En Debian:

```bash
sudo apt-get update
apt-cache search linux-headers-$(uname -r)
```

Ésto le indicará los archivos de cabecera disponibles. A continuación, por ejemplo:

```bash
sudo apt-get install kmod linux-headers-4.12.12-1-amd64
```

EJEMPLOS:

Todos los ejemplos en esta guía, están disponibles dentro del subdirectorio 'examples'. Para verificar que compilan:

```bash
cd examples
make
```

si existen errores de compilación, entonces, quizás tenga que actualizar su kernel, o instalar los ficheros de cabecera (headers) correspondientes.


HOLA MUNDO!

EL MÓDULO MÁS SENCILLO:

La mayor parte de las veces que se empieza a programar, es costumbre empezar con el programa "Hola mundo!". Se desconoce el fatal destino de las personas que rompen esta tradición, pero es más seguro no arriesgarse y descubrirlo. Así pues comenzaremos con una serie de programas "Hola mundo!", que demostraran los diferentes aspectos básicos al escribir un módulo del kernel.

A continuación presentamos el módulo más sencillo posible:

Hagamos un directorio para pruebas:

```bash
mkdir -p ~/develop/kernel/hello-1
cd ~/develop/kernel/hello-1
```

Copie y pegue el siguiente código en su editor de texto favorito, y guarde el archivo como hello-1.c

```C
/*
 *  hello-1.c - El módulo del kernel más sencillo del mundo! 
 */

#include <linux/module.h>       /* Requerido por todos los módulos */
#include <linux/kernel.h>       /* Requerido por KERN_INFO */

int init_module(void)
{
    printk(KERN_INFO "Hola mundo 1.\n");

    /*
     * Un valor distinto a 0 indica que int_module fallo; que el módulo no se ha podido cargar. 
     */
    return 0;
}

void cleanup_module(void)
{
    printk(KERN_INFO "Adiós mundo 1.\n");
}
```

Ahora hay que hacer un Makefile. Puede copiar y pegar el siguiente, pero cambie los tabuladores por espacios.

```bash
obj-m += hello-1.o

all:
        make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
        make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
```

Y finalmente, ejecute:

```
make
```

Si todo va bien, se debería haber compilado el módulo **hello-1.ko**. Para más información, se puede usar el comando:

```bash
sudo modinfo hello-1.ko
```

Y el siguiente comando:

```bash
sudo lsmod | grep hello
```

... no debería devolver nada. Puede probar a cargar su nuevo y reluciente módulo con:

```bash
sudo insmod hello-1.ko
```

El carácter de guión, se convertirá en un guión bajo, así que puede probar de nuevo:

```bash
sudo lsmode | grep hello
```

ahora debería poder ver su módulo cargado. Y puede ser retirado de nuevo con:

```bash
sudo rmmod hello_1
```

Dese cuenta que el guión fue reemplazado por un guión bajo. Para ver lo que ha pasado puede revisar los logs:

```bash
journalctl --since "1 hour ago" | grep kernel 
```

Ahora conoce los principios básicos para crear, compilar, instalar y borrar módulos; así que ahora nos centraremos cómo funciona un módulo.

Los módulos del kernel, han de poseer dos funciones al menos, una función de inicio, denominada **"init_module()"** que será llamada cuando el módulo sea inicializado por el kernel (insmode). Y una función de finalización denominada **"cleanup_module()"**
qué será llamada justo antes de que el módulo sea retirado del kernel (rmmod). Realmente estos procedimientos han
cambiado, empezando en la versión del kernel 2.3.13. Ahora mismo, se puede utilizar cualquier nombre para las 
funciones de inicio y final de un módulo, se explicará como en la Seccion 2.3. De hecho esté método es el mejor, y 
el indicado ahora mismo. De todos modos, aún así se siguen usando los nombres **init_module()** y **cleanup_module()**
para las funciones de inicio y final de los módulos.

Normalmente, **init_module()** o bién registra una referencia hacia alguna parte del kernel, o bién sustituye alguna
función del código con su própio código (habitualmente, esté código hace algo particular, y a continuación llama a la 
función original). La función **cleanup_module()** deberia deshacer lo que quiera que la función **init_module()** 
hiciera, de manera que el módulo sea retirado de forma segura.

Por último, cada módulo del kernel necesita incluir **'linux/module.h'**. También se ha de incluir el fichero
**'linux/kernel.h'**, para usar la macro de **printk()** y **KERN_ALERT**, que se explicará en la Sección 2.1.1.


